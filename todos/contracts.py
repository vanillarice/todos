from dataclasses import dataclass
from typing import List, Union

from slotomania.core import Contract, Undefined


@dataclass
class IdOnlyRequest(Contract):
    id: int


@dataclass
class TodoItemUpdateRequest(Contract):
    id: int
    description: str
    priority: int


@dataclass
class TodoItem(Contract):
    description: str
    id: int
    priority: int


@dataclass
class ServerState(Contract):
    TodoItem: List[TodoItem]  # type: ignore
