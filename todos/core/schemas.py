from marshmallow import fields, Schema


class RequestBodySchema(Schema):
    pass


class EmptyBodyRequest(Schema):
    pass


class IdOnlyRequest(Schema):
    id = fields.Int(required=True)


class TodoItemUpdateRequest(Schema):
    id = fields.Int(required=True)
    description = fields.Str(required=True)


class TodoItem(Schema):
    id = fields.Int(required=True)
    description = fields.Str(required=True)
    priority = fields.Int(required=False)


class ServerState(Schema):
    TodoItem = fields.List(fields.Nested(TodoItem()), required=True)
