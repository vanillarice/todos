import logging

from django.db import models

LOGGER = logging.getLogger(__name__)


class TodoItem(models.Model):
    description = models.CharField(max_length=1024)
    priority = models.IntegerField(default=0)
    created_ts = models.DateTimeField(auto_now_add=True)
    updated_ts = models.DateTimeField(auto_now=True)
