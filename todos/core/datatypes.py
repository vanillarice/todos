from enum import Enum, auto


class EntityTypes(Enum):
    TodoItem = auto()
