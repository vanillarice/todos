from django.test import TestCase
from django.urls import reverse
from todos.core.models import TodoItem


class ApiTestCase(TestCase):
    def test_TodoItemsLoad(self):
        todo = TodoItem.objects.create(description="test")
        response = self.client.post(
            reverse('api', kwargs={'endpoint': 'TodoItemsLoad'}))
        assert response.status_code == 200
        assert response.data == {
            "operations": [{
                "entity_type":
                "TodoItem",
                "target_value": [{
                    "description": "test",
                    "id": todo.id
                }],
                "verb":
                "MERGE"
            }],
            "errors":
            None,
            "redirect":
            ""
        }
