import logging
from typing import Callable

from slotomania.contrib.contracts import EmptyBodySchema
from slotomania.core import Instruction
from slotomania.core import InstructorView as BaseInstructorView
from slotomania.core import Operation
from slotomania.core import RequestResolver as BaseRequestResolver

from todos import contracts
from todos.core.datatypes import EntityTypes
from todos.core.models import TodoItem

LOGGER = logging.getLogger(__name__)


class RequestResolver(BaseRequestResolver):
    resolve: Callable[[], Instruction]


class TodoItemsLoad(RequestResolver):
    use_jwt_authentication = False
    data: EmptyBodySchema

    def resolve(self) -> Instruction:
        todos = TodoItem.objects.all()
        items = [
            contracts.TodoItem(
                id=item.id,
                description=item.description,
                priority=item.priority) for item in todos
        ]
        return Instruction(
            [Operation.MERGE_PREPEND(EntityTypes.TodoItem, items)])


class TodoItemDelete(RequestResolver):
    use_jwt_authentication = False
    data: contracts.IdOnlyRequest

    def resolve(self) -> Instruction:
        item = TodoItem.objects.get(id=self.data.id)
        item.delete()
        return Instruction([
            Operation.DELETE(
                EntityTypes.TodoItem,
                contracts.TodoItem(
                    id=self.data.id,
                    description=item.description,
                    priority=item.priority).asdict())
        ])


class TodoItemUpdate(RequestResolver):
    use_jwt_authentication = False
    data: contracts.TodoItemUpdateRequest

    def resolve(self) -> Instruction:
        id = self.data.id
        description = self.data.description
        priority = self.data.priority
        if not id:
            item = TodoItem.objects.create(
                description=description, priority=priority)
        else:
            item = TodoItem.objects.get(id=id)
            item.description = description
            item.priority = priority
            item.save()
        return Instruction([
            Operation.MERGE_PREPEND(EntityTypes.TodoItem, [
                contracts.TodoItem(
                    id=item.id,
                    description=item.description,
                    priority=item.priority)
            ])
        ])


class InstructorView(BaseInstructorView):
    routes = {
        "TodoItemsLoad": TodoItemsLoad,
        "TodoItemUpdate": TodoItemUpdate,
        "TodoItemDelete": TodoItemDelete
    }
