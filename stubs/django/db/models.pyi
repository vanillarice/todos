from typing import Any, Callable, ClassVar, Generic, Iterable, List, TypeVar

T = TypeVar('T')


class Model:
    # __init__: Callable[..., None]
    id: Any
    objects: ClassVar
    get_source_display: Callable
    save: Callable


Q: Any
F: Any
CASCADE: Any

IntegerField: Any
CharField: Any
DateField: Any
ForeignKey: Any
signals: Any
DateTimeField: Any
TextField: Any
Sum: Any
QuerySet: Any
DecimalField: Any
# Manager: Any
PROTECT: Any

# class QuerySet(Iterable[T]):
#     filter: Callable[..., 'QuerySet' [T]]


class Manager():
    from_queryset: Callable[..., Any]
    filter: Callable
    create: Callable
    get: Callable
    all: Callable
