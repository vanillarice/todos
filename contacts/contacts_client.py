from pprint import pprint
import sys
from typing import List

import requests

ENDPOINT = 'https://api.pagerduty.com/users'
HEADERS = {
    "Accept": "application/vnd.pagerduty+json;version=2",
    "Authorization": "Token token=y_NbAkKc66ryYTWUXYEu"
}

BASIC_FIELDS = ["id", "name"]


def get_users() -> List[dict]:
    response = requests.get(ENDPOINT, headers=HEADERS)
    users = response.json()["users"]
    return users


def get_details(user_id: str, users: List[dict]):
    for u in users:
        if u["id"] == user_id:
            return u


def list_contact_methods(user_detail: dict):
    results = []
    for contact_method in user_detail["contact_methods"]:
        link = contact_method["self"]
        response = requests.get(link, headers=HEADERS)
        data = response.json()["contact_method"]
        if data["type"] in ("sms_contact_method", "phone_contact_method"):
            address = "{}-{}".format(data["country_code"], data["address"])
        else:
            address = data["address"]

        results.append("{}: {}".format(
            data["type"].replace("_contact_method", ""), address))
        # results.append({
        #     "label": data["label"],
        #     "address": data["address"],
        #     "type": data["type"]
        # })

    print('\n'.join(results))


if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        command, target = args[1:]
        if command == 'get_details':
            users = get_users()
            details = get_details(target, users)
            list_contact_methods(details)
            sys.exit(0)

    for u in get_users():
        print(u["id"], u["name"])
