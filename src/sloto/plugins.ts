import {push} from "react-router-redux"

export const BASE_URL = `${window.location.protocol}//${
    window.location.hostname
}:${window.location.port ? "8000" : ""}/api`

export async function callSlotoEndpoint(request: {
    endpoint: string
    jsonBody: {}
    authToken?: string
}) {
    const url = `${BASE_URL}/${request.endpoint}/`
    const jwt = request.authToken
        ? request.authToken
        : sessionStorage.getItem("jwt")

    let call = await fetch(url, {
        method: "POST",
        body: JSON.stringify(request.jsonBody),
        // credentials: 'include',
        headers: {
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest",
            Authorization: jwt ? `JWT ${jwt}` : ""
        }
    })
    return call.json()
}

export const pushHistory = (
    pathname,
    criteria?: {},
    state: {} | null = null
) => {
    const searchString =
        criteria && criteria && Object.keys(criteria).length
            ? "?criteria=" + JSON.stringify(criteria)
            : ""
    return push({
        pathname,
        search: searchString,
        state
    })
}
export function callEndpoint(
    endpoint: string,
    jsonBody: {} = {},
    successCallback?: any
) {
    return async (dispatch, getState) => {
        dispatch({
            type: "callEndpoint",
            endpoint,
            ...jsonBody
        })
        const response = await callSlotoEndpoint({
            endpoint: endpoint,
            jsonBody: jsonBody,
            authToken: getState().serverState.jwt_auth_token
        })
        if (successCallback) {
            return successCallback(response)
        } else {
            dispatch({
                type: "UpdateServerStateAction",
                partialStateUpdate: response
            })
            return response
        }
    }
}
