import * as plugins from "./plugins"

export interface AuthenticateUserRequest {
    username: string
    password: string
}

export interface EmptyBodySchema {}

export interface IdOnlyRequest {
    id: number
}

export interface TodoItemUpdateRequest {
    id: number
    description: string
    priority: number
}

export interface TodoItem {
    description: string
    id: number
    priority: number
}

export interface ServerState {
    TodoItem: Array<TodoItem>
}

export function TodoItemsLoad(requestBody: EmptyBodySchema): any {
    return (dispatch) => {
        return dispatch(plugins.callEndpoint("TodoItemsLoad", requestBody))
    }
}

export function TodoItemUpdate(requestBody: TodoItemUpdateRequest): any {
    return (dispatch) => {
        return dispatch(plugins.callEndpoint("TodoItemUpdate", requestBody))
    }
}

export function TodoItemDelete(requestBody: IdOnlyRequest): any {
    return (dispatch) => {
        return dispatch(plugins.callEndpoint("TodoItemDelete", requestBody))
    }
}

export const SLOTO_ACTION_CREATORS = {
    TodoItemsLoad,
    TodoItemUpdate,
    TodoItemDelete
}
