import * as _ from "lodash"
import {combineReducers} from "redux"
import {routerReducer} from "react-router-redux"
import u from "updeep"
import {mergeArrays} from "rice-instructor"
import {
    Operation,
    State,
    UpdateServerStateAction,
    ServerState
} from "@src/types"

export const initialState: State = {
    serverState: {
        TodoItem: []
    }
}

function createReducer(initialState, handlers) {
    return function reducer(state = initialState, action) {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action)
        } else {
            return state
        }
    }
}

function applyOperation(result: ServerState, operation: Operation) {
    const targetValue = operation.target_value
    if (operation.verb === "OVERWRITE") {
        return u.updateIn(operation.entity_type, targetValue, result)
    } else if (operation.verb === "MERGE_APPEND") {
        const merged = mergeArrays(
            result[operation.entity_type],
            targetValue,
            "append"
        )
        return u.updateIn(operation.entity_type, merged, result)
    } else if (operation.verb === "MERGE_PREPEND") {
        const merged = mergeArrays(
            result[operation.entity_type],
            targetValue,
            "prepend"
        )
        return u.updateIn(operation.entity_type, merged, result)
    } else if (operation.verb === "DELETE") {
        return u.updateIn(
            operation.entity_type,
            u.reject((item) => {
                return item.id === targetValue.id
            }),
            result
        )
    } else {
        throw {operation}
    }
}

const serverState = createReducer(initialState.serverState, {
    UpdateServerStateAction: (
        serverState: ServerState,
        action: UpdateServerStateAction
    ) => {
        let ret = Object.assign({}, serverState)
        const operations = action.partialStateUpdate.operations
        ret = _.reduce(operations, applyOperation, ret)
        return ret
    }
})

const reducers = combineReducers({
    serverState,
    router: routerReducer
})

export default reducers
