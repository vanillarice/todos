import {SLOTO_ACTION_CREATORS} from "@src/sloto"

export const ACTIONS = {
    ...SLOTO_ACTION_CREATORS
}
