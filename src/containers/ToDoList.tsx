import * as React from "react"
import {connect} from "react-redux"
import {List, Divider} from "@material-ui/core"
import {bindActionCreators} from "redux"
import {State, ActionType, actionCreators, TodoItem} from "@src/types"
import {TodoListItem} from "@src/components/TodoItem"

interface TodoListState {
    newItem: TodoItem
}
interface ListProp {
    actions: ActionType
    todos?: Array<TodoItem>
}
class TodoListContainer extends React.Component<ListProp, TodoListState> {
    state = {
        newItem: {
            id: 0,
            description: "",
            priority: 0
        }
    }
    componentWillMount() {
        this.props.actions.TodoItemsLoad({})
    }
    deleteItem(id) {
        return this.props.actions.TodoItemDelete({
            id
        })
    }
    updateItem(newItem: TodoItem) {
        return this.props.actions.TodoItemUpdate(newItem)
    }
    render() {
        const todos = this.props.todos || []
        return (
            <div>
                <List component="nav">
                    {todos.map((todo) => (
                        <TodoListItem
                            key={todo.id}
                            item={todo}
                            updateItem={this.updateItem.bind(this)}
                            deleteItem={this.deleteItem.bind(this)}
                        />
                    ))}
                    <TodoListItem
                        key={"new"}
                        item={{id: 0, description: "", priority: 0}}
                        updateItem={this.updateItem.bind(this)}
                        deleteItem={this.deleteItem.bind(this)}
                    />
                </List>
                <Divider />
            </div>
        )
    }
}

const mapStateToProps = (state: State): {} => {
    return {todos: state.serverState.TodoItem}
}
const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    }
}

export const TodoList = connect(mapStateToProps, mapDispatchToProps)(
    TodoListContainer
)
