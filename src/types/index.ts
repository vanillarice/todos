import {ACTIONS as actionCreators} from "@src/actions"
import {ServerState} from "@src/sloto"

export type ActionType = typeof actionCreators

export interface State {
    serverState: ServerState
}
export {ACTIONS as actionCreators} from "@src/actions"
export * from "@src/sloto"

export interface Operation {
    verb: "MERGE_APPEND" | "MERGE_PREPEND" | "OVERWRITE" | "DELETE"
    entity_type: "TodoItem"
    target_value: any
}
export interface UpdateServerStateAction {
    type: "UpdateServerStateAction"
    partialStateUpdate: {
        operations: Array<Operation>
    }
}
