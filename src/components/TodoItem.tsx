import * as React from "react"
import * as _ from "lodash"
import {
    ListItem,
    ListItemSecondaryAction,
    IconButton,
    Grid
} from "@material-ui/core"
import DeleteIcon from "@material-ui/icons/Delete"
import TextField from "@material-ui/core/TextField"
import Checkbox from "@material-ui/core/Checkbox"
import {TodoItem} from "@src/types"

export interface ItemProp {
    item: TodoItem
    updateItem: (newItem: TodoItem) => any
    deleteItem: (id: number) => any
}
export class TodoListItem extends React.Component<ItemProp> {
    deleteItem(event) {
        if (!this.props.item.id) {
            throw this.props.item
        }
        this.props.deleteItem(this.props.item.id)
    }
    updatePriority = (priority: string) => {
        this.props.updateItem(
            Object.assign({}, this.props.item, {priority: +priority})
        )
    }
    updateDescription = (description: string) => {
        this.props.updateItem(Object.assign({}, this.props.item, {description}))
    }
    render() {
        const {item} = this.props
        const bouncedPriority = _.debounce(this.updatePriority, 500)
        const bouncedDescription = _.debounce(this.updateDescription, 500)
        return (
            <ListItem>
                <Checkbox tabIndex={-1} disableRipple />
                <Grid container>
                    <Grid item xs={1}>
                        <TextField
                            type="number"
                            InputProps={{disableUnderline: true}}
                            fullWidth
                            defaultValue={this.props.item.priority}
                            onChange={(event) =>
                                bouncedPriority(event.target.value)
                            }
                        />
                    </Grid>
                    <Grid item xs={3}>
                        <TextField
                            InputProps={{disableUnderline: true}}
                            fullWidth
                            defaultValue={this.props.item.description}
                            onChange={(event) =>
                                bouncedDescription(event.target.value)
                            }
                        />
                    </Grid>
                </Grid>
                {item.id ? (
                    <ListItemSecondaryAction>
                        <IconButton aria-label="Comments">
                            <DeleteIcon onClick={this.deleteItem.bind(this)} />
                        </IconButton>
                    </ListItemSecondaryAction>
                ) : null}
            </ListItem>
        )
    }
}
