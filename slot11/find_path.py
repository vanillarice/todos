from typing import Dict, List

# visits = (1, 3, 2, 1, 3, 2)


def find_paths(visits: list) -> Dict[tuple, int]:
    results: dict = {}
    for iterator in range(len(visits) - 2):
        triple = tuple(visits[iterator:iterator + 3])
        # print(triple)
        if triple in results:
            results[triple] += 1
        else:
            results[triple] = 1

    return results


def parse_user_logs(user_visits: Dict[str, list]):
    results = {}
    for user in user_visits:
        paths = find_paths(user_visits[user])
        for path, count in paths.items():
            if path in results:
                results[path] += count
            else:
                results[path] = count

    return results


def find_max(path_counts: Dict[tuple, int]):
    max = sorted(path_counts.items(), key=lambda x: x[1], reverse=True)[0]
    max_count = max[1]
    return [
        path_count for path_count in path_counts.items()
        if path_count[1] == max_count
    ]


# print(find_paths([1, 3, 2, 1, 3, 2]))
# print(parse_user_logs({"C1": [1, 3, 2, 1, 3, 2], "C2": [1, 3, 2]}))


def get_input(filename) -> Dict[str, list]:
    with open(filename) as f:
        inputs: dict = {}
        for line in f:
            timestamp, customer, page = line.split(",")
            # print(timestamp.strip(), customer.strip(), page.strip())
            customer = customer.strip()
            if customer in inputs:
                inputs[customer].append(page.strip())
            else:
                inputs[customer] = [page.strip()]

    return inputs


# inputs1 = get_input()
inputs2 = get_input("input2.txt")
results = parse_user_logs(inputs2)

print(find_max(results))
