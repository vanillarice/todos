import os

import django
from slotomania.core import Contract, ReduxAction, contracts_to_typescript


def main() -> None:
    os.environ.setdefault("DJANGO_SETTINGS_MODULE",
                          "todos.settings.deploy_settings")
    django.setup()
    from todos.api.views import InstructorView
    from todos import contracts  # NOQA

    ts_output_file = 'src/sloto/index.ts'

    excluded = ["ReduxAction", "Instruction", "Operation", "Verbs"]
    ts_code = '\n' + contracts_to_typescript(
        dataclasses=[
            c for c in Contract.__subclasses__() if c.__name__ not in excluded
        ],
        redux_actions=[
            ReduxAction(
                name=name,
                contract=endpoint.__annotations__["data"],
                pre_action=endpoint.pre_action,
                callback=endpoint.callback)
            for name, endpoint in InstructorView.routes.items()
        ],
    )
    with open(ts_output_file, 'w') as f:
        f.write(ts_code)


if __name__ == '__main__':
    main()
