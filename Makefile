SERVICES := web db

.PHONY: psql
psql:
	docker-compose exec db psql -U postgres todos

.PHONY: test
test: ## Run tests
	docker-compose exec web pytest $(filter-out $@,$(MAKECMDGOALS))

.PHONY: dropschema
dropschema:
	docker-compose exec db psql -U postgres todos -c 'drop schema public cascade; create schema public'

.PHONY: start-dev
start-dv:
	docker-compose up -d && docker-compose logs -f


.PHONY: ssh
ssh: ## Launch a shell inside the container
	docker-compose exec web bash

.PHONY: ${SERVICES}
${SERVICES}:
	docker-compose exec $@ $(filter-out $@,$(MAKECMDGOALS))

.PHONY: lint
lint:
	rm -r stubs/slotomania
	stubgen --recursive -o stubs/ slotomania
	fix_sloto_stubs.py `ag class.*Contract stubs/ -l`
	docker-compose exec web mypy .

.PHONY: sloto
sloto:
	python sloto.py
	prettier src/sloto/index.ts --write
