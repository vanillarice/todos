Django
psycopg2
djangorestframework
django-extensions
six
django-cors-middleware
requests
djangorestframework-jwt
uwsgi
dj-database-url
mypy
django-debug-toolbar
slotomania==0.1.1
marshmallow==3.0.0b8


flake8
yapf
ipython
pytest
pytest-django
pytest-cov
isort
